package main

import (
	"os"

	"gitlab.com/kittiza/xTunnel/app"
)

func main() {

	file, err := os.Open("config.json")
	if err != nil {
		panic(err)

	}
	xtn := app.New(file)
	xtn.SetSSH(app.SSH{
		Address:  "103.70.6.240:22",
		Username: "root",
		Password: "620916XVer@407318",
	})

	_, err = xtn.ConnectSSH()
	if err != nil {
		xtn.Log("ConnectSSH error: %s\n", err)

		return
	}
	xtn.ServeWeb()
	xtn.Listen()
}
