package app

import (
	"io"
	"net"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"golang.org/x/crypto/ssh"
)

//Tunnel เปิด Tunnel และสร้างการเชื่อมต่อ
func (x *XTunnel) Tunnel(sx *ssh.Client, name, serverListen, localRemote string) {
	sv, err := sx.Listen("tcp", serverListen)
	if err != nil {
		x.Log("[%s] SSH Listen error: %s\n", name, err)

		return
	}

	x.Log("[%s] SSH Listen: On %s\n", name, sv.Addr())

	x.Listens = append(x.Listens, listens{
		Name:     name,
		Listener: sv,
		Config: config{
			Name:        name,
			ServerLisen: serverListen,
			LocalRemote: localRemote,
		},
	})
	defer sv.Close()
	var wg sync.WaitGroup
	for i := 0; i <= 1000; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			for {
				svconn, err := sv.Accept()
				if err != nil {
					if err == io.EOF {

						break
					}
					x.Log("[%s] sv Accept error: %s\n", name, err)
					continue
				}
				x.Log("[%s] Accept: %s\n", name, svconn.RemoteAddr())
				loconn, err := net.Dial("tcp", localRemote)
				if err != nil {
					svconn.Close()
					x.Log("[%s] loconn Dial error: %s\n", name, err)
					continue
				}

				x.Log("[%s] loconn LocalAddr: %s\n", name, loconn.LocalAddr())
				x.Log("[%s] loconn RemoteAddr: %s\n", name, loconn.RemoteAddr())

				copyConn := func(writer, reader net.Conn) {
					_, err := io.Copy(writer, reader)
					if err != nil {
						x.Log("[%s] Copy: %s", name, err)
					}
					writer.Close()
					reader.Close()
				}

				go copyConn(loconn, svconn)
				go copyConn(svconn, loconn)

			}
			wg.Done()
		}(&wg)
	}
	wg.Wait()
}

//TunnelAll TunnelAll
func (x *XTunnel) TunnelAll(sx *ssh.Client) {
	for _, v := range x.Config {
		go func(v config) {
			for {
				if mainSSH == nil {
					x.ConnectSSH()
					time.Sleep(1 * time.Second)
					continue
				}
				ss, err := mainSSH.NewSession()
				if err != nil {
					x.Log("[NewSession] err:%s", err)
					x.ConnectSSH()
					continue
				}
				ss.Run("kill -9 $(lsof -t -i:" + strings.Split(v.ServerLisen, ":")[1] + ")")
				ss.Close()
				go x.Tunnel(mainSSH, v.Name, v.ServerLisen, v.LocalRemote)
				break
			}
		}(v)

	}
}

//Listen Listen
func (x *XTunnel) Listen() {
	x.TunnelAll(mainSSH)
	go x.checkLive()
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt, syscall.SIGTERM)
	func(c chan os.Signal) {
		<-c
		x.Shuttdow(mainSSH)

		os.Exit(0)
	}(sigc)
}

//Shuttdow Shuttdow
func (x *XTunnel) Shuttdow(sx *ssh.Client) {

	for _, v := range x.Listens {

		v.Listener.Close()
		x.Log("[%s] shutting dow.", v.Name)
	}
	x.Listens = make([]listens, 0)
	x.Log("Caught signal : xTunnel shutting down.")
	sx.Close()
}

func (x *XTunnel) checkLive() {
	for {

		_, _, err := mainSSH.SendRequest("keepalive@openssh.com", true, nil)

		if err != nil {
			if err.Error() != "request failed" {
				x.Log("Disconnect err:%s", err)
				x.ConnectSSH()
				x.Shuttdow(mainSSH)
				x.TunnelAll(mainSSH)
				x.Log("%s", "Reconnect.")

			}

		}

		time.Sleep(1 * time.Second)
	}
}
