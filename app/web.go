package app

import (
	"crypto/subtle"
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

//ServeWeb เปิดเซิร์ฟเวอร์ของ web
func (x *XTunnel) ServeWeb() {
	go x.webRouter()
}
func (x *XTunnel) webRouter() {
	http.Handle("/", http.FileServer(http.Dir("./web")))
	http.HandleFunc("/config", basicAuth(x.apiConfig, "admin", "nimda", "Please enter your username and password for this site"))
	x.Log("%s", "Serving at localhost:8000...")
	log.Fatal(http.ListenAndServe(":8000", nil))
}

func (x *XTunnel) apiConfig(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	type webConfig struct {
		Name  string `json:"name"`
		IP    string `json:"ip"`
		Port  string `json:"port"`
		Addr  string `json:"addr"`
		Local string `json:"local"`
	}
	xs := make([]webConfig, 0)
	for _, x := range x.Config {
		s := strings.Split(x.ServerLisen, ":")
		port := s[1]
		n := webConfig{
			Name:  x.Name,
			IP:    "103.70.6.240",
			Port:  port,
			Addr:  "103.70.6.240:" + port,
			Local: x.LocalRemote,
		}
		xs = append(xs, n)
	}
	json.NewEncoder(w).Encode(xs)
}
func basicAuth(handler http.HandlerFunc, username, password, realm string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		user, pass, ok := r.BasicAuth()

		if !ok || subtle.ConstantTimeCompare([]byte(user), []byte(username)) != 1 || subtle.ConstantTimeCompare([]byte(pass), []byte(password)) != 1 {
			w.Header().Set("WWW-Authenticate", `Basic realm="`+realm+`"`)
			w.WriteHeader(401)
			w.Write([]byte("Unauthorised.\n"))
			return
		}

		handler(w, r)
	}
}
